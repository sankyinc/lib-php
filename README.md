# SankyNet PHP Libraries

[![Latest Stable Version](https://poser.pugx.org/sankynet/lib-php/v/stable.png)](https://packagist.org/packages/sankynet/lib-php) [![Total Downloads](https://poser.pugx.org/sankynet/lib-php/downloads.png)](https://packagist.org/packages/sankynet/lib-php) [![Build Status](https://travis-ci.com/sankyinc/lib-php.svg?branch=master)](https://travis-ci.com/bitbucket/sankyinc/lib-php)

Available namespaces:

+ [`SankyNet\API`](SankyNet/API/README.md)
+ [`SankyNet\Core`](SankyNet/Core/README.md)
