<?php

use PHPUnit\Framework\TestCase;
use SankyNet\API\Payments\GatewayController;
use SankyNet\API\Payments\Gateways\Authorize;
use SankyNet\Core\NVPSubmitter;

class GatewayControllerAuthorizeTest extends TestCase
{

  protected $auth_aim;
  protected $gc;
  protected $verbose = false;

  private $common_params = array(
    'login'      => 'TestAuthorize',
    'tran_key'   => '4784343hjvhefdv2323',
    'first_name' => 'sanky',
    'last_name'  => 'test',
    'email'      => 'test@sankynet.com',
    'street'     => '123 Sample Drive, 2nd Floor',
    'city'       => 'New York',
    'state'      => 'NY',
    'zip_code'   => '10036',
    'telephone'  => '347-742-3478'
  );

  protected function setUp(): void
  {
    $this->auth_aim = new Authorize();
    $this->gc = new GatewayController($this->auth_aim, 'test');
  }

  protected function tearDown(): void
  {
    $this->auth_aim = null;
    $this->gc = null;
    \Mockery::close();
  }

  private function echoDiag(&$object, $method)
  {
    if ($this->verbose) {
      echo $method . ":\n\n";
      echo "Parameters: " . print_r($object->getParams(), true) . "\n";
      echo "Response: " . print_r($object->getResponse(), true) . "\n";
      echo "Error: " . print_r($object->getError(), true) . "\n";
    }
  }

  public function testGetterAndFilters()
  {
    $params = array(
      'account'    => '3782-8224-6310_005',
      'amount'     => '$ 1,203.76',
      'expiration' => '09-16'
    );

    $params = array_merge($this->common_params, $params);
    $this->gc->set($params);

    foreach ($params as $name => $value) {
      switch ($name) {
        case 'first_name' : $this->assertEquals('Sanky', $this->gc->{$name}); break;
        case 'last_name'  : $this->assertEquals('Test', $this->gc->{$name}); break;
        case 'account'    : $this->assertEquals('378282246310005', $this->gc->{$name}); break;
        case 'amount'     : $this->assertEquals('1203.76', $this->gc->{$name}); break;
        case 'expiration' : $this->assertEquals('0916', $this->gc->{$name}); break;
        default           : $this->assertEquals($value, $this->gc->{$name});
      }
    }
  }

  public function testSetter()
  {
    // There should not be any params at the beginning.
    $this->assertEmpty($this->gc->getParams());

    // Wrong email address.
    $this->gc->email = 'abc.def.com';
    $this->assertNull($this->gc->email);

    $this->gc->expiration = '082015';
    $this->assertNull($this->gc->expiration);

    // Good email address.
    $this->gc->email = 'abc@def.com';
    $this->assertNotEmpty($this->gc->email);

    // Good expiration date.
    $this->gc->expiration = '0815';
    $this->assertNotEmpty($this->gc->expiration);

    // Now should be ok and there are no warnings.
    $this->assertNotEmpty($this->gc->getParams());
    $this->assertEquals(2, count($this->gc->getParams()));
  }

  public function testGetterExceptions()
  {
    $this->expectError();
    $this->expectErrorMessage('Undefined property via __get()');
    $this->gc->non_existing_parameter;
  }

  public function testTransactionRequests()
  {
    date_default_timezone_set('America/New_York');
    $responses = array();
    $this->auth_aim = new Authorize();
    $this->gc = new GatewayController($this->auth_aim);
    $this->gc->set($this->common_params);

    // We will simulate NVPSubmitter's behaviour since we don't want to do
    // real curl communication. We will mock only sendRequest() method
    // to always return true (successfull curl communication) and getResponse()
    // to have some responses (obviously, since there is no real curl call).
    $sub_responses = array(
      'doAuthorization' => "1|1|1|This transaction has been approved.|V6DLHJ|Y|2221871729|||5.00|CC|auth_only||Sanky|Test||599 11 Ave, 6th Floor|New York|NY|10036|United States|||||||||||||||||965A0093425EBED3EA6A62AE33FDA3E6||2|||||||||||XXXX0005|American Express||||||||||||||||",
      'doCreditCardSaleTransaction' => "1|1|1|This transaction has been approved.|IW5FVY|Y|2211047186|||5.00|CC|auth_capture||Sanky|Test||599 11 Ave, 6th Floor|New York|NY|10036|United States|||||||||||||||||992BEF46C23C39E573A94608CF1C9FD2||2|||||||||||XXXX0005|American Express||||||||||||||||",
      'voidCreditCardTransaction' => "1|1|1|This transaction has been approved.|HFIOYX|P|2211045181|||0.00|CC|void||||||||10036||||||||||||||||||0AF10078CAD51D82D5ED57585B8A04DA|||||||||||||XXXX0005|American Express||||||||||||||||"
    );

    // Create Submitter mock for further tests.
    $submitter = \Mockery::mock(new NVPSubmitter);
    $submitter->shouldReceive('sendRequest')->andReturn(true);
    $submitter->shouldReceive('getResponse')->andReturnValues($sub_responses);

    // Let's set this mock object as a submitter for gateway controller.
    $this->gc->setSubmitter($submitter);

    // doCreditCardSaleTransaction
    $method = 'doAuthorization';
    $params = array(
      'account' => '3782-8224-631-0005',
      'amount' => '$5.00',
      'expiration' => '1216'
    );

    $this->gc->set($params);
    $this->assertTrue($this->gc->$method());
    $this->echoDiag($this->gc, $method);
    $response = $this->gc->getResponse();
    $this->assertNotEmpty($response, 'No response: ' . $method);
    $this->assertEmpty($this->gc->getError());
    $responses[] = array($method, $response);

    // doCreditCardSaleTransaction
    $method = 'doCreditCardSaleTransaction';
    $params = array(
      'account' => '3782-8224-631-0005',
      'amount' => '$5.00',
      'expiration' => '1216'
    );

    $this->gc->set($params);
    $this->assertTrue($this->gc->$method());
    $this->echoDiag($this->gc, $method);
    $response = $this->gc->getResponse();
    $this->assertNotEmpty($response, 'No response: ' . $method);
    $this->assertEmpty($this->gc->getError());
    $responses[] = array($method, $response);

    // voidCreditCardTransaction
    $method = 'voidCreditCardTransaction';
    $this->gc->original_id = $response['Transaction ID'];
    $this->assertTrue($this->gc->$method());
    $this->echoDiag($this->gc, $method);
    $this->assertNotEmpty($this->gc->getResponse(), 'No response: ' . $method);
    $this->assertEmpty($this->gc->getError());
    $responses[] = array($method, $this->gc->getResponse());

    return $responses;
  }

  /**
   * @dataProvider testTransactionRequests
   */
  public function testTransactionResponses($method, $params)
  {
    $this->assertEquals(isset($params['Response Code']), true);
    $this->assertEquals(isset($params['Response Subcode']), true);
    $this->assertEquals(isset($params['Response Reason Code']), true);
    $this->assertEquals(isset($params['Response Reason Text']), true);
  }
}
