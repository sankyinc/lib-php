<?php

use PHPUnit\Framework\TestCase;
use SankyNet\Core\Model;

class ModelTest extends TestCase
{
  public function testModelMagicMethods()
  {
    $model = new Model;
    $name = "Matt";
    $model->name = $name;
    $this->assertEquals($model->name, $name);

    $return = $model->name;
    $this->assertEquals($model->name, $return);

    $this->assertEquals(isset( $model->name ), TRUE);
    $this->assertEquals(isset( $model->first_name ), FALSE);

    $array = $model->toArray();
    $this->assertIsArray($array);
    $this->assertArrayHasKey("name", $array);
    $this->assertTrue($array["name"] == $name);

    $json = $model->toJSON();
    $this->assertJsonStringEqualsJsonString(json_encode($array), $json);

    unset($model->name);
    $this->assertEquals(isset( $model->name ), FALSE);

  }
}
