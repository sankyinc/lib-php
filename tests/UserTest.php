<?php

use PHPUnit\Framework\TestCase;
use SankyNet\Core\User;

class UserTest extends TestCase
{
  public function testUserExtendsModel()
  {
    $matt = new User;
    $email = "mryan@sankynet.com";
    $matt->email = $email;
    $this->assertEquals($matt->email, $email);
  }

  public function testUserEmptyAttribute()
  {
    $user = new User;
    $should_be_null = $user->email;
    $this->assertNull($should_be_null);
  }
}
