<?php

use PHPUnit\Framework\TestCase;
use SankyNet\Core\NVPSubmitter;

class NVPSubmitterTest extends TestCase
{
  protected function setUp(): void
  {
    $this->submitter = new NVPSubmitter();
  }

  public function testParamsSet()
  {
    $this->submitter->my_param = 'test_string';
    $this->assertEquals('test_string', $this->submitter->my_param);
    $this->submitter->clear();
  }

  /**
   * @expectedException PHPUnit_Framework_Error
   */
  public function testParamRemoveUnset()
  {
    $this->submitter->setServlet('http://example.com');
    $this->submitter->my_param = 'test_string';
    $this->submitter->remove('my_param');
    $this->expectError();
    $this->expectErrorMessage('Undefined property via __get()');
    $this->submitter->my_param;
    $this->submitter->never_set_param;
  }

  public function testHelperMethods()
  {
    $this->submitter->setServlet('http://example.com');
    $this->submitter->setResponseFormat('responseFormat', 'json');
    $this->submitter->PARAM = 'test';
    $this->assertEmpty($this->submitter->getDiagMessage());
    $this->assertNotEmpty($this->submitter->debugNVP());
  }

  public function testSendRequest()
  {
    $this->assertFalse($this->submitter->sendRequest(),
      'Successfull execution of sendRequest() without required parameters.');
    $this->assertStringContainsString($this->submitter->getDiagMessage(), 'Error: missing parameters.');

    $this->submitter->setServlet('/'); // 100% non-existing-domain
    $this->submitter->test_param = 'test_param';
    $this->assertFalse($this->submitter->sendRequest(),
      'Successfull execution of sendRequest() with invalid servlet.');
    $this->assertStringContainsString($this->submitter->getDiagMessage(), 'Error: Unable to perform cURL session.');
  }

  public function testGetResponse()
  {
    $this->assertEmpty($this->submitter->getResponse());
  }
}
