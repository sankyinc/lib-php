<?php

use PHPUnit\Framework\TestCase;
use SankyNet\API\Payments\GatewayController;
use SankyNet\API\Payments\Gateways\PayFlow;
use SankyNet\Core\NVPSubmitter;

class GatewayControllerPayFlowTest extends TestCase
{

  protected $payflow;
  protected $gc;
  protected $verbose = false;

  private $common_params = array(
    'vendor'     => 'TestPFP',
    'partner'    => 'PayPal',
    'user'       => 'TestPFP',
    'password'   => 'my@very893secret!%3pass',
    'first_name' => ' sanky & sanky " ',
    'last_name'  => 'test',
    'email'      => 'test@sankynet.com',
    'street'     => '123 Sample Drive, 2nd Floor',
    'city'       => 'New York',
    'state'      => 'NY',
    'zip_code'   => '10036',
    'telephone'  => '347-742-3478',
    'currency'   => 'USD'
  );

  protected function setUp(): void
  {
    $this->payflow = new PayFlow();
    $this->gc = new GatewayController($this->payflow, 'test');
  }

  protected function tearDown(): void
  {
    $this->payflow = null;
    $this->gc = null;
    \Mockery::close();
  }

  private function echoDiag(&$object, $method)
  {
    if ($this->verbose) {
      echo $method . ":\n\n";
      echo "Parameters: " . print_r($object->getParams(), true) . "\n";
      echo "Response: " . print_r($object->getResponse(), true) . "\n";
      echo "Error: " . print_r($object->getError(), true) . "\n";
    }
  }

  public function testGetterAndFilters()
  {
    $params = array(
      'account'    => '3782-8224-6310_005',
      'amount'     => '$ 1,203.76',
      'expiration' => '09-16',
      'pay_period' => 'qter ',
    );

    $params = array_merge($this->common_params, $params);
    $this->gc->set($params);

    foreach ($params as $name => $value) {
      switch ($name) {
        case 'first_name' : $this->assertEquals(' Sanky & Sanky " ', $this->gc->{$name}); break;
        case 'last_name'  : $this->assertEquals('Test', $this->gc->{$name}); break;
        case 'account'    : $this->assertEquals('378282246310005', $this->gc->{$name}); break;
        case 'amount'     : $this->assertEquals('1203.76', $this->gc->{$name}); break;
        case 'expiration' : $this->assertEquals('0916', $this->gc->{$name}); break;
        case 'pay_period' : $this->assertEquals('QTER', $this->gc->{$name}); break;
        default           : $this->assertEquals($value, $this->gc->{$name});
      }
    }
  }

  public function testSetter()
  {
    // There should not be any params at the beginning.
    $this->assertEmpty($this->gc->getParams());

    // Wrong email address.
    $this->gc->email = 'abc.def.com';
    $this->assertNull($this->gc->email);

    $this->gc->expiration = '082015';
    $this->assertNull($this->gc->expiration);

    // Good email address.
    $this->gc->email = 'abc@def.com';
    $this->assertNotEmpty($this->gc->email);

    // Good expiration date.
    $this->gc->expiration = '0815';
    $this->assertNotEmpty($this->gc->expiration);

    // Now should be ok and there are no warnings.
    $this->assertNotEmpty($this->gc->getParams());
    $this->assertEquals(2, count($this->gc->getParams()));
  }

  public function testGetterExceptions()
  {
    $this->expectError();
    $this->expectErrorMessage('Undefined property via __get()');
    $this->gc->non_existing_parameter;
  }

  public function testAlterParamsForRequest()
  {
    $params = $this->payflow->alterParamsForRequest($this->common_params);

    foreach ($params as $name => $value) {
      if (strpos($name, 'first_name') !== false) {
        $found = true;
        $this->assertEquals($name, 'first_name[13]');
        $this->assertEquals($value, 'sanky & sanky');
      } else {
      }
    }
    $this->assertFalse(empty($found), 'Tested param is not set for the test.');
  }

  public function testTransactionRequests()
  {
    date_default_timezone_set('America/New_York');
    $responses = array();
    $this->payflow = new PayFlow();
    $this->gc = new GatewayController($this->payflow);
    $this->gc->set($this->common_params);

    // We will simulate NVPSubmitter's behaviour since we don't want to do
    // real curl communication. We will mock only sendRequest() method
    // to always return true (successfull curl communication) and getResponse()
    // to have some responses (obviously, since there is no real curl call).
    $sub_responses = array(
      'doCreditCardSaleTransaction' => array(
        'RESULT'     => '0',
        'PNREF'      => 'A13N5A07F628',
        'RESPMSG'    => 'Approved',
        'AUTHCODE'   => '019169',
        'AVSADDR'    => 'N',
        'AVSZIP'     => 'Y',
        'HOSTCODE'   => '000',
        'RESPTEXT'   => 'AP',
        'PROCAVS'    => 'Z',
        'IAVS'       => 'N',
        'PREFPSMSG'  => 'No Rules Triggered',
        'POSTFPSMSG' => 'No Rules Triggered'
      ),
      'voidCreditCardTransaction' => array(
        'RESULT'   => '0',
        'PNREF'    => 'A73N4EC556BC',
        'RESPMSG'  => 'Approved',
        'HOSTCODE' => '000'
      ),
      'addCreditCardRecurringProfile' => array(
        'RESULT'    => '0',
        'RPREF'     => 'R7354EC556C0',
        'PROFILEID' => 'RT0000000120',
        'RESPMSG'   => 'Approved'
      ),
      'setExpressCheckoutSaleTransaction' => array(
        'RESULT'        => '0',
        'RESPMSG'       => 'Approved',
        'TOKEN'         => 'EC-7YR80487P0011590T',
        'CORRELATIONID' => 'd7694043938e8'
      )
    );
    $sub_getResponse = array();

    foreach ($sub_responses as $response) {
      $sub_getResponse[] = $this->composeNvpString($response);
    }
    $s = new NVPSubmitter();
    // Create Submitter mock for further tests.
    $submitter = \Mockery::mock(new NVPSubmitter);
    $submitter->shouldReceive('sendRequest')->andReturn(true);
    $submitter->shouldReceive('getResponse')->andReturnValues($sub_getResponse);

    // Let's set this mock object as a submitter for gateway controller.
    $this->gc->setSubmitter($submitter);

    // doCreditCardSaleTransaction
    $method = 'doCreditCardSaleTransaction';
    $params = array(
      'account' => '3782-8224-631-0005',
      'amount' => '$5.00',
      'expiration' => '1216'
    );

    $this->gc->set($params);
    $this->assertTrue($this->gc->$method());
    $this->echoDiag($this->gc, $method);
    $response = $this->gc->getResponse();
    $this->assertNotEmpty($response, 'No response: ' . $method);
    $this->assertEmpty($this->gc->getError());
    $responses[] = array($method, $response);

    // voidCreditCardTransaction
    $method = 'voidCreditCardTransaction';
    $this->gc->original_id = $response['PNREF'];
    $this->assertTrue($this->gc->$method());
    $this->echoDiag($this->gc, $method);
    $this->assertNotEmpty($this->gc->getResponse(), 'No response: ' . $method);
    $this->assertEmpty($this->gc->getError());
    $responses[] = array($method, $this->gc->getResponse());

    // addCreditCardRecurringProfile
    $method = 'addCreditCardRecurringProfile';
    $params = array(
      'profile_name' => 'sanky_test',
      'start' => date('mdY', mktime(0, 0, 0, date('n'), (date('j') + 1), date('Y'))),
      'pay_period' => 'mont'
    );
    $this->gc->set($params);
    $this->assertTrue($this->gc->$method());
    $this->echoDiag($this->gc, $method);
    $response = $this->gc->getResponse();
    $this->assertNotEmpty($response, 'No response: ' . $method);
    $this->assertEmpty($this->gc->getError());
    $responses[] = array($method, $response);

    // setExpressCheckoutSaleTransaction
    $method = 'setExpressCheckoutSaleTransaction';
    $params = array(
      'amount' => '$ 5.01',
      'cancel_url' => 'http://www.sankynet.com',
      'return_url' => 'http://www.sankynet.com'
    );
    $this->gc->set(array_merge($this->common_params, $params), true);
    $this->assertTrue($this->gc->$method());
    $this->echoDiag($this->gc, $method);
    $response = $this->gc->getResponse();
    $this->assertNotEmpty($response, 'No response: ' . $method);
    $this->assertEmpty($this->gc->getError());
    $responses[] = array($method, $response);

    return $responses;
  }

  /**
   * @dataProvider testTransactionRequests
   */
  public function testTransactionResponses($method, $params)
  {
    $this->assertEquals(isset($params['RESULT']), true);
    if ($method == 'setExpressCheckoutSaleTransaction') {
      $this->assertNotEmpty($params['TOKEN']);
    }
  }

  protected function composeNvpString($params) {
    $tokens = array();
    foreach ($params as $key => $value) {
      $tokens[] = $key . '=' . $value;
    }
    return implode('&', $tokens);
  }
}
