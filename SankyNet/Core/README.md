# SankyNet Core

Base classes and extensions

Available Classes:

+ [`SankyNet\Core\Model`](#model)
+ [`SankyNet\Core\User`](#user)
+ [`SankyNet\Core\NVPSubmitter`](#nvpsubmitter)

## Model

```php
$model = new Model;
$name = "Matt";
$model->name = $name;
$array = $model->toArray();
$json = $model->toJSON();
```

## User

Extends `Model`

```php
$matt = new User;
$email = "mryan@sankynet.com";
$matt->email = $email;
```

## NVPSubmitter

```php
$params = array(
  'my_param_1' => 'value_1',
  'my_param_2' => 'value_2',
);

$submitter = new NVPSubmitter('http://servlet.com', $params);
try {
  $result = $submitter->sendRequest();
} catch (Exception $e) {
  // Handle an exception
  echo $e->getCode() . ' - ' . $e;
}

if (!empty($result)) {
  // Handle a response
  $response = $submitter->getResponse();
}
```
