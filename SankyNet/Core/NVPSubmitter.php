<?php

namespace SankyNet\Core;

use SankyNet\Core\Model;

/**
 * Provides request NVP formatting and submit services using CURL.
 *
 * This class converts any array or stdClass object into
 * name-value (NVP) pairs called here tokens and glue the with '&' character
 * to construct the request string that can be submitted through CURL upon request.
 * It also provides some basic formatting of the response from the server
 * ex. from NVP response string to array or object.
 * XML is not supported at this time unless it is the original
 * server response format.
 *
 * Usage:
 *   $submit = new NVPSubmitter($servlet, $params);
 *   $submit->setResponseFormat('response_format', 'json');
 *   $result = $submit->sendRequest();
 *   $response = $submit->getResponse();
 * where:
 *   $servlet - is and endpoint where the request will be submitted
 *     e.x. 'https://example.com/endpoint'
 *   $params - is an associative array or object
 *     which is used to compose the request.
 *
 * sendRequest() returns TRUE if cURL session was established and FALSE on error.
 *   It reflects internal status after executing the function and is independed
 *   from the status of the response from the resource. getResponse() returns
 *   the response from resource regardless of its status.
 */
class NVPSubmitter
{

  private $servlet;
  private $params;
  private $response_format;
  private $output_format;

  private $request;
  private $response;

  private $diag_message;

  /**
   * Class Constructor.
   *
   * @param string $servlet Resource servlet url
   * @param array  $params  Params
   */
  public function __construct($servlet = '', $params = array())
  {
    $this->setServlet($servlet);
    $this->set($params);
  }

  /**
   * Set the value of the property.
   *
   * @param string $property Property name.
   * @param mixed  $value    Property value.
   */
  public function __set($property, $value)
  {
    $this->params[$property] = $value;
  }

  /**
   * Get the value of the property.
   *
   * @param  string $property Property name.
   * @return mixed            Value of the property.
   */
  public function __get($property)
  {
    if (isset($this->params[$property])) {
      return $this->params[$property];
    }

    $trace = debug_backtrace();
    trigger_error(
      'Undefined property via __get(): ' . $property .
      ' in ' . $trace[0]['file'] .
      ' on line ' . $trace[0]['line'],
      E_USER_NOTICE);
    return null;
  }

  /**
   * Unset a param from the list.
   *
   * @param  string $key Param name.
   * @return void
   */
  public function __unset($key)
  {
    if (isset($this->params[$key])) {
      unset($this->params[$key]);
    }
    $this->createNVP();
  }

  /**
   * Set Servlet URL.
   *
   * @param string $servlet Servlet URL
   */
  public function setServlet($servlet = '')
  {
    $this->servlet = $servlet;
  }

  public function setResponseFormat($key, $format = 'array')
  {
    switch ($format) {
      case 'array':  $this->response_format = 'json'; $this->output_format = 'array'; break;
      case 'object': $this->response_format = 'json'; $this->output_format = 'object'; break;
      case 'json':   $this->response_format = 'json'; $this->output_format = 'json'; break;
      case 'xml':    $this->response_format = 'xml';  $this->output_format = 'xml'; break;
      default:       $this->response_format = 'json'; $this->output_format = 'array'; break;
    }
    if (empty($this->params[$key]) || $this->params[$key] != $this->response_format) {
      $this->params[$key] = $this->response_format;
      $this->createNVP();
    }
  }

  /**
   * Set multiple params at a time.
   *
   * @param array   $params Parmas list in object or array format.
   * @param boolean $reset  Should the list of existing params be cleared before?
   */
  public function set($params = array(), $reset = true)
  {
    $params = $this->toArray($params);
    if ($reset) {
      $this->params = $params;
    } else {
      foreach ($params as $key => $value) {
        $this->params[$key] = $value;
      }
    }
    $this->createNVP();
  }

  /**
   * Create request string.
   *
   * @return void
   */
  private function createNVP()
  {
    $this->request = '';
    if (!empty($this->params)) {
      foreach ($this->params as $key => $value) {
        $this->request .= $key.'='.$value.'&';
      }
      $this->request = rtrim($this->request, '&');
    }
  }

  /**
   * Get request string.
   *
   * @return string
   */
  public function debugNVP()
  {
    return $this->request;
  }

  /**
   * Unset a param from the list.
   *
   * @param  string $key Param name.
   * @return void
   */
  public function remove($key)
  {
    $this->__unset($key);
  }

  /**
   * Clear the params list and the request string.
   *
   * @return void
   */
  public function clear()
  {
    $this->params = array();
    $this->request = '';
  }

  /**
   * Submit the request to the servlet.
   *
   * @param  array   $params Parameters list.
   * @return boolean         True if submitted succesfully, false otherwise.
   */
  public function sendRequest($params = array())
  {
    $result = false;
    if (!empty($params)) {
      $this->set($params);
    }

    if ($this->checkRequirements()) {
      // Uses the CURL library for php to establish a connection,
      // submit the post, and record the response.
      if (function_exists('curl_init') && extension_loaded('curl')) {
        // Initiate curl object
        $curl = curl_init($this->servlet);
        if ($curl === FALSE) {
          $this->diag_message = 'Error: Unable to initialize cURL session.';
          return false;
        }
        // Set to 0 to eliminate header info from response
        curl_setopt($curl, CURLOPT_HEADER, 0);
        // Returns response data instead of true(1)
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        // Use HTTP POST to send the data
        curl_setopt($curl, CURLOPT_POSTFIELDS, $this->request);
        // Set to 0 or FALSE if you don't want to verify peer (not recommended).
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1);
        // Execute curl post and store results in $this->response
        $curl_response = curl_exec($curl);
        curl_close($curl); // close curl object
        // Additional options may be required depending upon your server configuration
        // you can find documentation on curl options at http://www.php.net/curl_setopt
        if ($curl_response === FALSE) {
          $this->diag_message = 'Error: Unable to perform cURL session.';
        } else {
          // Get the response.
          $this->response = $curl_response;
          $this->diag_message = 'Success: request processed successfully.';
          $result = true;
        }
      } else {
        $this->diag_message = 'Error: CURL module not installed.';
      }
    } else {
      $this->diag_message = 'Error: missing parameters.';
    }

    return $result;
  }

  /**
   * Check basic requirements for request submission.
   *
   * @return boolean
   */
  private function checkRequirements()
  {
    if (empty($this->params) || empty($this->servlet)) {
      return false;
    }
    return true;
  }

  /**
   * Get servlet response.
   *
   * @return mixed Formatted or raw response from the servlet.
   */
  public function getResponse()
  {
    if (!empty($this->response)) {
      switch ($this->output_format) {
        case 'array':  return json_decode($this->response, true);
        case 'object': return json_decode($this->response);
        case 'json':
        case 'xml':    return $this->response;
        default:       return $this->response;
      }
    } else {
      return $this->getDiagMessage();
    }
  }

  /**
   * Convert object to array.
   *
   * @param  object $object Data's object representation.
   * @return array          Data's array representation.
   */
  private function toArray($object)
  {
    $model = new Model();
    foreach ($object as $key => $value) {
      $model->{$key} = $value;
    }
    return $model->toArray();
  }

  /**
   * Get diagnostic message.
   *
   * @return string
   */
  public function getDiagMessage()
  {
    return $this->diag_message;
  }

}
