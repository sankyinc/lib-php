<?php

namespace SankyNet\Core;

/**
 * Model Class
 */
class Model
{
  private $properties = array();

  public function __get($key)
  {
    return (isset($this->properties[$key])) ? $this->properties[$key] : null;
  }

  public function __set($key, $value)
  {
    $this->properties[$key] = $value;
  }

  public function __isset($key)
  {
    return isset($this->properties[$key]);
  }

  public function __unset($key)
  {
    unset($this->properties[$key]);
  }

  private function convertToArray($properties) {
		$ret = array();
		foreach($properties as $k => $v) {
			if($v instanceof Model) {
				$ret[$k] = $v->toArray();
			} else if (is_array($v)) {
				$ret[$k] = $this->convertToArray($v);
			} else {
				$ret[$k] = $v;
			}
		}
		return $ret;
	}

  public function toArray() {
    return $this->convertToArray($this->properties);
  }

  public function toJSON() {
    return json_encode($this->toArray());
  }

}
