<?php

namespace SankyNet\API;

use SankyNet\Core\User;
use LSS\XML2Array;
use LSS\Array2XML;

/**
 * Silverpop API Wrapper
 *
 * Engage 8.8
 *
 * @package Sanky
 * @version dev-master
 * @author Matt Ryan <mryan@sankynet.com>
 */
class Silverpop
{

  private $user_name;
  private $password;
  private $host;
  private $port = 80;
  private $request_xml;
  private $list_id;

  public $request;
  public $response;
  public $jsessionid;
  public $organization_id;

  public $error;

  /**
   * Builds an API connection, stores a session id for use with future requests.
   *
   * @param mixed $user_name
   * @param mixed $password
   * @param mixed $host
   * @access public
   * @return void
   */
  public function __construct($user_name, $password, $host, $list_id)
  {
    if (is_null($user_name) or is_null($password) or is_null($host) or is_null($list_id)) {
      throw new \InvalidArgumentException;
    }

    $this->user_name = $user_name;
    $this->password = $password;
    $this->host = $host;
    $this->list_id = $list_id;

    $this->request = array(
      'USERNAME' => $user_name,
      'PASSWORD' => $password
    );

    $this->submit("Login");

    if (isset($this->response['Envelope']['Body']['RESULT']['SESSIONID'])) {
      $this->jsessionid       = ";jsessionid=" . $this->response['Envelope']['Body']['RESULT']['SESSIONID'];
    }
    if (isset($this->response['Envelope']['Body']['RESULT']['ORGANIZATION_ID'])) {
      $this->organization_id  = $this->response['Envelope']['Body']['RESULT']['ORGANIZATION_ID'];
    }
  }

  /**
   * Converts an array to a Silverpop valid XML object.
   * This requires there be two wrapping tags, "Body", and "Envelope".
   *
   * @param mixed $data
   * @param mixed $action
   * @static
   * @access private
   * @return void
   */
  private static function prepare($data, $action)
  {
    $silverpop_wrapper = array(
      'Body' => array(
        $action => $data
      )
    );

    return Array2XML::createXML('Envelope', $silverpop_wrapper);
  }

  /**
   * The request submission.
   *
   * @param mixed $data
   * @access private
   * @return void
   */
  private function submit($action)
  {
    $this->request_xml = self::prepare($this->request, $action)->saveXML();

    $request = curl_init();
    curl_setopt($request, CURLOPT_URL, $this->host . $this->jsessionid);
    curl_setopt($request, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($request, CURLOPT_FORBID_REUSE, TRUE);
    curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($request, CURLOPT_POST, TRUE);
    curl_setopt($request, CURLOPT_POSTFIELDS, "xml=" . urlencode($this->request_xml));
    $response = curl_exec($request);

    if (! $response) {
      throw new \DomainException("Invalid response from Silverpop.");
    }

    $this->response = XML2Array::createArray($response);
    $this->reportErrors($this->response);
  }

  /**
   * Extract the error information from the response if any exists.
   *
   * @param array $response
   * @return void
   */
  protected function reportErrors($response)
  {
    if ($response['Envelope']['Body']['RESULT']['SUCCESS'] === "false") {
      $this->error = array(
        'message' => $response['Envelope']['Body']['Fault']['FaultString']['@cdata'],
        'details' => $response['Envelope']['Body']['Fault']['detail']['error'],
        'log' => serialize($response['Envelope']['Body']['Fault'])
      );
    } else {
      $this->error = array();
    }
  }

  /**
   * getListMetaData
   *
   * @access public
   * @return void
   */
  public function getListMetaData()
  {
    $this->request = array(
      'LIST_ID' => $this->list_id
    );

    $this->submit("GetListMetaData");
  }

  /**
   * addOrUpDateRecipient
   *
   * @param User $user
   * @access public
   * @return void
   */
  public function addOrUpDateRecipient(User $user, $sync_fields = array())
  {
    $this->request = array(
      'LIST_ID' => $this->list_id,
      'CREATED_FROM' => '1',
      'UPDATE_IF_FOUND' => 'true',
      'COLUMN' => array()
    );

    foreach ($user->toArray() as $key => $value) {
      $this->request['COLUMN'][] = array(
        'NAME' => $key,
        'VALUE' => $value
      );
    }

    if (! empty($sync_fields)) {
      $this->request['SYNC_FIELDS'] = array();
      foreach ($sync_fields as $key => $value) {
        $this->request['SYNC_FIELDS']['SYNC_FIELD'][] = array(
          'NAME' => $key,
          'VALUE' => $value
        );
      }
    }

    $this->submit("AddRecipient");
  }

  /**
   * selectRecipientData
   *
   * Sets response to the returned User object
   *
   * @param $email
   * @access public
   * @return User $user
   */
  public function selectRecipientData($email, $lookup_columns = array())
  {
    $this->request = array(
      'LIST_ID' => $this->list_id,
      'EMAIL' => $email
    );

    if (! empty($lookup_columns)) {
      foreach ($lookup_columns as $key => $value) {
        $this->request['COLUMN'][] = array(
          'NAME' => $key,
          'VALUE' => $value
        );
      }
    }

    $this->submit("SelectRecipientData");
    $response = new User;

    if ($this->response['Envelope']['Body']['RESULT']['SUCCESS'] !== "false") {
      $response->email = $this->response['Envelope']['Body']['RESULT']['Email'];
      $response->id = $this->response['Envelope']['Body']['RESULT']['RecipientId'];
      $response->optedIn = $this->response['Envelope']['Body']['RESULT']['OptedIn'];
      $response->optedOut = $this->response['Envelope']['Body']['RESULT']['OptedOut'];
      foreach ($this->response['Envelope']['Body']['RESULT']['COLUMNS']['COLUMN'] as $value) {
        $response->__set($value['NAME'], $value['VALUE']);
      }
      return $this->response = $response;
    } else {
      $response->email = $email;
      return $this->response = $response;
    }
  }

  /**
   * addContactToContactList
   *
   * It's possible to add a contact to a contact list via the
   * AddRecipient method, but that's messy. Use this instead.
   *
   * @param mixed $contact_id
   * @param mixed $contact_list_id
   * @access public
   * @return void
   */
  public function addContactToContactList($contact_id, $contact_list_id)
  {
    $this->request = array(
      'CONTACT_LIST_ID' => $contact_list_id,
      'CONTACT_ID' => $contact_id
    );

    $this->submit("AddContactToContactList");
  }

}
