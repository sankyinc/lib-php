<?php

namespace SankyNet\API\Payments\Gateways;

use SankyNet\API\Payments\Gateway;
use SankyNet\API\Payments\GatewayInterface;

class PayFlow extends Gateway implements GatewayInterface
{

  protected $config = array(
    'servlets' => array(
      'test'=>'https://pilot-payflowpro.paypal.com',
      'live'=>'https://payflowpro.paypal.com'
    ),

    'params' => array(
      // Connection parameters
      'VENDOR' => array(
        'aliases' => array('vendor'),
        'required' => true,
        'default' => 'PayPal'
      ),
      'PARTNER' => array(
        'aliases' => array('partner'),
        'required' => true
      ),
      'USER' => array(
        'aliases' => array('user'),
        'required' => true
      ),
      'PWD' => array(
        'aliases' => array('password'),
        'required' => true
      ),

      // Request parameters
      'TRXTYPE' => array(
        'aliases' => array('transaction_type'),
        'filters' => array('filterTrxtype')
      ),
      'TENDER' => array(
        'aliases' => array('tender'),
        'filters' => array('filterTender')
      ),
      'ACTION' => array(
        'aliases' => array('action'),
        'filters' => array('filterAction')
      ),
      'START' => array(
        'aliases' => array('start')
      ),
      'TERM' => array(
        'aliases' => array('payments_count')
      ),
      'PROFILENAME' => array(
        'aliases' => array('profile_name')
      ),
      'PAYPERIOD' => array(
        'aliases' => array('pay_period'),
        'filters' => array('filterPayperiod')
      ),
      'BILLINGTYPE' => array(
        'aliases' => array('billing_type')
      ),
      'TOKEN' => array(
        'aliases' => array('token')
      ),
      'PAYERID' => array(
        'aliases' => array('payer_id')
      ),
      'PAYMENTTYPE' => array(
        'aliases' => array('payment_type')
      ),
      'ORIGID' => array(
        'aliases' => array('original_id')
      ),
      'OPTIONALTRX' => array(
        'aliases' => array('optional_trx'),
        'filters' => array('filterOptionalTrx')
      ),
      'OPTIONALTRXAMT' => array(
        'aliases' => array('optional_trx_amt'),
        'filters' => array('filterAmount')
      ),

      // Payment parameters
      'NAME' => array( // Name on the card
        'aliases' => array('name'),
        'filters' => array('ucwords')
      ),
      'ACCT' => array(
        'aliases' => array('account'),
        'filters' => array('filterAcct')
      ),
      'EXPDATE' => array(
        'aliases' => array('expiration'),
        'filters' => array('filterExpdate')
      ),
      'AMT' => array(
        'aliases' => array('amount'),
        'filters' => array('filterAmt')
      ),
      'CVV2' => array(
        'aliases' => array('cvv2')
      ),
      'CURRENCY' => array(
        'aliases' => array('currency'),
        'filters' => array('strtoupper')
      ),
      'BAID' => array(
        'aliases' => array('baid')
      ),

      // Purchaser parameters
      'FIRSTNAME' => array(
        'aliases' => array('first_name'),
        'filters' => array('ucwords')
      ),
      'LASTNAME' => array(
        'aliases' => array('last_name'),
        'filters' => array('ucwords')
      ),
      'EMAIL' => array(
        'aliases' => array('email'),
        'filters' => array('filterEmail')
      ),
      'STREET' => array(
        'aliases' => array('street')
      ),
      'CITY' => array(
        'aliases' => array('city')
      ),
      'STATE' => array(
        'aliases' => array('state')
      ),
      'ZIP' => array(
        'aliases' => array('zip_code')
      ),
      'COUNTRY' => array(
        'aliases' => array('country')
      ),
      'PHONENUM' => array(
        'aliases' => array('telephone', 'phone', 'contact_phone')
      ),

      // Other
      'COMMENT1' => array(
        'aliases' => array('comment1')
      ),
      'COMMENT2' => array(
        'aliases' => array('comment2')
      ),
      'ORDERDESC' => array(
        'aliases' => array('order_descr')
      ),
      'BA_DESC' => array(
        'aliases' => array('ba_descr')
      ),
      'RETURNURL' => array(
        'aliases' => array('return_url')
      ),
      'CANCELURL' => array(
        'aliases' => array('cancel_url')
      ),
      'VERBOSITY' => array(
        'aliases' => array('verbosity'),
        'default' => 'MEDIUM',
        'common' => true
      ),
      'CUSTIP' => array(
        'aliases' => array('custip'),
        'default' => '',
        'common' => true
      )
    ),

    'methods' => array(
      'doCreditCardAuthorizationTransaction' => array(
        'required' => array('TENDER', 'TRXTYPE', 'ACCT', 'EXPDATE', 'AMT'),
        'values' => array(
          'TENDER' => 'C',
          'TRXTYPE' => 'A'
        )
      ),
      'doCreditCardSaleTransaction' => array(
        'required' => array('TENDER', 'TRXTYPE', 'ACCT', 'EXPDATE', 'AMT'),
        'values' => array(
          'TENDER' => 'C',
          'TRXTYPE' => 'S'
        )
      ),
      'addCreditCardRecurringProfile' => array(
        'required' => array('TENDER', 'TRXTYPE', 'ACTION', 'PROFILENAME', 'ACCT', 'AMT', 'START', 'TERM', 'PAYPERIOD'),
        'values' => array(
          'TENDER' => 'C',
          'TRXTYPE' => 'R',
          'ACTION' => 'A',
          'TERM' => '0'
        )
      ),
      'voidCreditCardTransaction' => array(
        'required' => array('TENDER', 'TRXTYPE', 'ORIGID'),
        'values' => array(
          'TENDER' => 'C',
          'TRXTYPE' => 'V',
        )
      ),
      'setExpressCheckoutSaleTransaction' => array(
        'required' => array('TENDER', 'TRXTYPE', 'ACTION', 'AMT', 'CURRENCY', 'RETURNURL', 'CANCELURL'),
        'values' => array(
          'TENDER' => 'P',
          'TRXTYPE' => 'S',
          'ACTION' => 'S'
        )
      ),
      'getExpressCheckoutDetails' => array(
        'required' => array('TENDER', 'TRXTYPE', 'ACTION', 'TOKEN'),
        'values' => array(
          'TENDER' => 'P',
          'TRXTYPE' => 'A',
          'ACTION' => 'G'
        )
      ),
      'doExpressCheckoutSaleTransaction' => array(
        'required' => array('TENDER', 'TRXTYPE', 'ACTION', 'AMT', 'TOKEN'),
        'values' => array(
          'TENDER' => 'P',
          'TRXTYPE' => 'S',
          'ACTION' => 'D'
        )
      ),
      'setExpressCheckoutRecurringProfile' => array(
        'required' => array('TENDER', 'TRXTYPE', 'ACTION', 'AMT', 'CURRENCY', 'RETURNURL', 'CANCELURL', 'BA_DESC', 'PAYMENTTYPE'),
        'values' => array(
          'TENDER' => 'P',
          'TRXTYPE' => 'A',
          'ACTION' => 'S',
          'BILLINGTYPE' => 'MerchantInitiatedBilling',
          'PAYMENTTYPE' => 'any'
        )
      ),
      'createCustomerBillingAgreement' => array(
        'required' => array('TENDER', 'TRXTYPE', 'ACTION', 'TOKEN'),
        'values' => array(
          'TENDER' => 'P',
          'TRXTYPE' => 'A',
          'ACTION' => 'X',
        )
      ),
      'addExpressCheckoutRecurringProfile' => array(
        'required' => array('TENDER', 'TRXTYPE', 'ACTION', 'BAID', 'AMT', 'PAYPERIOD', 'START', 'TERM', 'PROFILENAME'),
        'values' => array(
          'TENDER' => 'P',
          'TRXTYPE' => 'R',
          'ACTION' => 'A',
        )
      ),
      'voidExpressCheckoutTransaction' => array(
        'required' => array('TENDER', 'TRXTYPE', 'ORIGID'),
        'values' => array(
          'TENDER' => 'P',
          'TRXTYPE' => 'V',
        )
      ),
      'doCustomRequest' => array(
        'required' => array('TENDER', 'TRXTYPE')
      ),
    )
  );

  /**
   * Class constructor.
   */
  public function __construct($servlets = array())
  {
    parent::__construct($servlets);
    $this->config['params']['CUSTIP']['default'] =
      isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1');
  }

  /**
   * Allow for final params alteration before they go to submitter.
   *
   * This alteration is usually specific to the particular gateway
   * and all gateways must implement it even if they leave params untouched.
   *
   * @param array $params Origianl param array.
   *
   * @return array Modified/original array.
   */
  public function alterParamsForRequest($params) {
    $altered = array();
    foreach($params as $key => $value) {
      $value = trim(str_replace('"', '', $value));
      $key = $key . '[' . strlen($value) . ']';
      $altered[$key] = $value;
    }
    return $altered;
  }

  /**
   * Format response.
   *
   * @param  string $result Raw response from gateway.
   * @param  string $format Format identifier ('array' or 'object').
   * @return void
   */
  public function formatResponse($result, $format = 'array')
  {
    if (! empty($result)) {
      $response = $format == 'array' ? array() : new StdClass();
      $tokens = explode('&', $result);
      foreach ($tokens as $token) {
        $pair = explode('=', $token);
        if ($format == 'array') {
          $response[$pair[0]] = isset($pair[1]) ? $pair[1] : null;
        } else {
          $response->{$pair[0]} = isset($pair[1]) ? $pair[1] : null;
        }
      }
      return $response;
    }
  }

  /**
   * Filters
   */

  /**
   * Transaction Type filter (TRXTYPE).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterTrxtype($value)
  {
    $value = strtoupper($value);
    $allowed = array(
      'S', // Sale
      'R', // Recurring
      'C', // Credit
      'A', // Authorization
      'D', // Delayed Capture
      'V', // Void
      'F', // Voice Authorization
      'I', // Inquiry
      'N', // Duplicate Transaction,
      'O'  // Order
    );

    if (!in_array($value, $allowed)) {
      return false;
    }

    return $value;
  }

  /**
   * Tender filter (TENDER).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterTender($value)
  {
    $value = strtoupper($value);
    $allowed = array(
      'A', // Automated clearinghouse
      'C', // Credit card
      'D', // Pinless debit
      'K', // Telecheck
      'P'  // PayPal
    );

    if (!in_array($value, $allowed)) {
      return false;
    }

    return $value;
  }

  /**
   * Action filter (ACTION).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterAction($value)
  {
    $value = strtoupper($value);
    $allowed = array(
      'A', // Add
      'M', // Modify
      'R', // Reactivate
      'C', // Cancel
      'I', // Inquiry
      'P', // Payment

      'S', // Set Express Checkout,
      'G', // Get Express Checkout Details
      'X', // Obtain Billing Agreement ID (BAID)
      'D', // Do Express Checkout Transaction
    );

    if (!in_array($value, $allowed)) {
      return false;
    }

    return $value;
  }

  /**
   * Optional Transaction filter (OPTIONALTRX).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterOptionalTrx($value)
  {
    $value = strtoupper($value);
    $allowed = array(
      'A', // Authorization
      'S', // Sale
    );

    if (!in_array($value, $allowed)) {
      return false;
    }

    return $value;
  }

  /**
   * Account Number filter (ACCT).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterAcct($value)
  {
    $value = preg_replace('/[^0-9]/', '', trim($value));
    if (!is_numeric($value)) {
      return false;
    }

    return (string) $value;
  }

  /**
   * Expiration Date filter (EXPDATE).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterExpdate($value)
  {
    $value = preg_replace('/[^0-9]/', '', trim($value));
    if (!is_numeric($value) || strlen($value) != 4) {
      return false;
    }

    return $value;
  }

  /**
   * Amount filter (AMT, OPTIONALTRXAMT).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterAmt($value)
  {
    $value = str_replace(array('-', '_', ' ', '$', ','), array(''), $value);
    $value = trim($value);
    if (!is_numeric($value)) {
      return false;
    }

    return $value;
  }

  /**
   * Pay Period filter (PAYPERIOD).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterPayperiod($value)
  {
    $value = strtoupper($value);
    $allowed = array(
      'WEEK', // Weekly
      'BIWK', // Every Two Weeks
      'SMMO', // Twice Every Month
      'FRWK', // Every Four Weeks (every 28 days)
      'MONT', // Mothly
      'QTER', // Quarterly
      'SMYR', // Twice Every Year
      'YEAR'  // Yearly
    );

    $value = trim($value);
    if (!in_array($value, $allowed)) {
      return false;
    }

    return $value;
  }

  /**
   * Email filter (EMAIL).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterEmail($value)
  {
    $value = strtolower($value);
    return filter_var($value, FILTER_VALIDATE_EMAIL);
  }
}
