<?php

namespace SankyNet\API\Payments\Gateways;

use SankyNet\API\Payments\Gateway;
use SankyNet\API\Payments\GatewayInterface;

class Authorize extends Gateway implements GatewayInterface
{

  protected $config = array(
    'servlets' => array(
      'test'=>'https://test.authorize.net/gateway/transact.dll',
      'live'=>'https://secure.authorize.net/gateway/transact.dll'
    ),

    'params' => array(
      // Connection parameters
      'x_login' => array(
        'aliases' => array('login', 'user'),
        'required' => true
      ),
      'x_tran_key' => array(
        'aliases' => array('tran_key', 'password'),
        'required' => true
      ),
      'x_version' => array(
        'aliases' => array('version'),
        'default' => '3.1',
        'common' => true
      ),
      'x_delim_data' => array(
        'aliases' => array('delim_data'),
        'required' => true,
        'default' => 'TRUE',
        'common' => true,
        'filters'=> array('filterDelimData')
      ),
      'x_delim_char' => array(
        'aliases' => array('delim_char'),
        'default' => '|',
        'common' => true
      ),
      'x_relay_response' => array(
        'aliases' => array('relay_response'),
        'default' => 'FALSE',
        'common' => true
      ),
      'x_customer_ip' => array(
        'aliases' => array('action'),
        'default' => null,
        'common' => true
      ),
      'x_duplicate_window' => array(
        'aliases' => array('duplicate_window'),
        'default' => '120',
        'common' => true
      ),
      'x_type' => array(
        'aliases' => array('type'),
        'filters' => array('filterType')
      ),
      'x_method' => array(
        'aliases' => array('method')
      ),
      'x_invoice_num' => array(
        'aliases' => array('invoice_num')
      ),
      'x_trans_id' => array(
        'aliases' => array('trans_id', 'original_id')
      ),
      'x_amount' => array(
        'aliases' => array('amount'),
        'filters' => array('filterAmt')
      ),
      'x_card_num' => array(
        'aliases' => array('card_num', 'account'),
        'filters' => array('filterAcct')
      ),
      'x_exp_date' => array(
        'aliases' => array('exp_date', 'expiration'),
        'filters' => array('filterExpdate')
      ),
      'x_card_code' => array(
        'aliases' => array('card_code', 'cvv2')
      ),
      'x_description' => array(
        'aliases' => array('description')
      ),
      'x_first_name' => array(
        'aliases' => array('first_name'),
        'filters' => array('ucwords')
      ),
      'x_last_name' => array(
        'aliases' => array('last_name'),
        'filters' => array('ucwords')
      ),
      'x_company' => array(
        'aliases' => array('company')
      ),
      'x_address' => array( // Name on the card
        'aliases' => array('address', 'street'),
        'filters' => array('ucwords')
      ),
      'x_city' => array(
        'aliases' => array('city'),
        'filters' => array('ucwords')
      ),
      'x_state' => array(
        'aliases' => array('state')
      ),
      'x_zip' => array(
        'aliases' => array('zip', 'zip_code')
      ),
      'x_country' => array(
        'aliases' => array('country')
      ),
      'x_phone' => array(
        'aliases' => array('telephone')
      ),
      'x_fax' => array(
        'aliases' => array('fax')
      ),
      'x_email' => array(
        'aliases' => array('email'),
        'filters' => array('filterEmail')
      ),
      'x_ship_to_first_name' => array(
        'aliases' => array('ship_to_first_name'),
        'filters' => array('ucwords')
      ),
      'x_ship_to_last_name' => array(
        'aliases' => array('ship_to_last_name'),
      ),
      'x_ship_to_company' => array(
        'aliases' => array('ship_to_company')
      ),
      'x_ship_to_address' => array(
        'aliases' => array('ship_to_address')
      ),
      'x_ship_to_city' => array(
        'aliases' => array('ship_to_city')
      ),
      'x_ship_to_state' => array(
        'aliases' => array('ship_to_state')
      ),
      'x_ship_to_zip' => array(
        'aliases' => array('ship_to_zip')
      ),
      'x_ship_to_country' => array(
        'aliases' => array('ship_to_country')
      ),
      'x_email_customer' => array(
        'aliases' => array('email_customer')
      )
    ),

    'methods' => array(
      'doAuthorization' => array(
        'required' => array('x_type', 'x_amount', 'x_card_num', 'x_exp_date'),
        'values' => array(
          'x_type' => 'AUTH_ONLY'
        )
      ),
      'doCreditCardSaleTransaction' => array(
        'required' => array('x_type', 'x_amount', 'x_card_num', 'x_exp_date'),
        'values' => array(
          'x_type' => 'AUTH_CAPTURE',
        )
      ),
      'voidCreditCardTransaction' => array(
        'required' => array('x_type', 'x_trans_id'),
        'values' => array(
          'x_type' => 'VOID'
        )
      ),
      'doCustomRequest' => array(
        'required' => array('x_type')
      ),
    )
  );

  /**
   * Class constructor.
   */
  public function __construct($servlets = array())
  {
    parent::__construct($servlets);
    $this->config['params']['x_customer_ip']['default'] =
      isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1';
  }

  /**
   * Allow for final params alteration before they go to submitter.
   *
   * This alteration is usually specific to the particular gateway
   * and all gateways must implement it even if they leave params untouched.
   *
   * @param array $params Origianl param array.
   *
   * @return array Modified/original array.
   */
  public function alterParamsForRequest($params) {
    return $params;
  }

  /**
   * Format response.
   *
   * @param  string $result Raw response from gateway.
   * @param  string $format Format identifier ('array' or 'object').
   * @return void
   */
  public function formatResponse($result, $format = 'array')
  {
    if (! empty($result)) {
      $response = $format == 'array' ? array() : new StdClass();
      $tokens = explode('|', $result);
      // Please see: http://developer.authorize.net/guides/AIM/wwhelp/wwhimpl/js/html/wwhelp.htm#href=4_TransResponse.6.2.html
      // for possible keys and values
      $keys = array(
        'Response Code',
        'Response Subcode',
        'Response Reason Code',
        'Response Reason Text',
        'Authorization Code',
        'AVS Response',
        'Transaction ID',
        'Invoice Number',
        'Description',
        'Amount',
        'Method',
        'Transaction Type',
        'Customer ID',
        'First Name',
        'Last Name',
        'Company',
        'Address',
        'City',
        'State',
        'ZIP Code',
        'Country',
        'Phone',
        'Fax',
        'Email Address',
        'Ship To First Name',
        'Ship To Last Name',
        'Ship To Company',
        'Ship To Address',
        'Ship To City',
        'Ship To State',
        'Ship To ZIP Code',
        'Ship To Country',
        'Tax',
        'Duty',
        'Freight',
        'Tax Exempt',
        'Purchase Order Number',
        'MD5 Hash',
        'Card Code Response',
        'Cardholder Authentication Verification Response',
        'Field 41',
        'Field 42',
        'Field 43',
        'Field 44',
        'Field 45',
        'Field 46',
        'Field 47',
        'Field 48',
        'Field 49',
        'Field 50',
        'Account Number',
        'Card Type',
        'Split Tender ID',
        'Requested Amount',
        'Balance On Card'
      );

      $counter = 0;
      foreach ($tokens as $token) {
        $index = isset($keys[$counter]) ? $keys[$counter] : 'Field ' . ($counter + 1);
        if ($format == 'array') {
          $response[$index] = $token;
        } else {
          $response->{$index} = $token;
        }
        $counter++;
      }
      return $response;
    }
  }

  /**
   * Filters
   */

  /**
   * Filter Delim Data (x_delim_data).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterDelimData($value)
  {
    $value = strtoupper($value);
    $allowed = array(
      'TRUE',
      'T',
      'YES',
      'FALSE',
      'F',
      'NO'
    );

    if (!in_array($value, $allowed)) {
      return false;
    }

    return $value;
  }

  /**
   * Filter Transaction Type (x_type).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterType($value)
  {
    $value = strtoupper($value);
    $allowed = array(
      'AUTH_CAPTURE',
      'AUTH_ONLY',
      'CAPTURE_ONLY',
      'CREDIT',
      'PRIOR_AUTH_CAPTURE',
      'VOID'
    );

    if (!in_array($value, $allowed)) {
      return false;
    }

    return $value;
  }

  /**
   * Account Number filter (ACCT).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterAcct($value)
  {
    $value = preg_replace('/[^0-9]/', '', trim($value));
    if (!is_numeric($value)) {
      return false;
    }

    return (string) $value;
  }

  /**
   * Expiration Date filter (EXPDATE).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterExpdate($value)
  {
    $value = preg_replace('/[^0-9]/', '', trim($value));
    if (!is_numeric($value) || strlen($value) != 4) {
      return false;
    }

    return $value;
  }

  /**
   * Amount filter (AMT, OPTIONALTRXAMT).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterAmt($value)
  {
    $value = str_replace(array('-', '_', ' ', '$', ','), array(''), $value);
    $value = trim($value);
    if (!is_numeric($value)) {
      return false;
    }

    return $value;
  }

  /**
   * Email filter (EMAIL).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterEmail($value)
  {
    $value = strtolower($value);
    return filter_var($value, FILTER_VALIDATE_EMAIL);
  }
}
