<?php

namespace SankyNet\API\Payments\Gateways;

use SankyNet\API\Payments\Gateway;
use SankyNet\API\Payments\GatewayInterface;

class PayPal extends Gateway implements GatewayInterface
{

  protected $config = array(
    'servlets' => array(
      'test'=>'https://api-3t.sandbox.paypal.com/nvp',
      'live'=>'https://api-3t.paypal.com/nvp'
    ),

    'params' => array(
      'VENDOR' => array(
        'aliases' => array('vendor'),
        'required' => true,
        'default' => 'PayPal'
      ),
      'PARTNER' => array(
        'aliases' => array('partner'),
        'required' => true
      ),
      'USER' => array(
        'aliases' => array('user'),
        'required' => true
      ),
      'PWD' => array(
        'aliases' => array('password'),
        'required' => true
      ),
      'SIGNATURE' => array(
        'aliases' => array('signature'),
        'required' => true
      ),
      'VERSION' => array(
        'aliases' => array('ver', 'version'),
        'common' => true,
        'default' => '95.0'
      ),
      'METHOD' => array(
        'aliases' => array('method')
      ),
      'PAYMENTREQUEST_0_PAYMENTACTION' => array(
        'aliases' => array('payment_action'),
        'filters' => array('filterPaymentAction')
      ),
      'PAYMENTREQUEST_0_AMT' => array(
        'aliases' => array('amount'),
        'filters' => array('filterAmt')
      ),
      'AMT' => array(
        'aliases' => array('amount_conf'),
        'filters' => array('filterAmt')
      ),
      'PAYMENTREQUEST_0_DESC' => array(
        'aliases' => array('description')
      ),
      'PAYMENTREQUEST_0_CURRENCYCODE' => array(
        'aliases' => array('currency'),
        'filters' => array('strtoupper')
      ),
      'MAXFAILEDPAYMENTS' => array(
        'aliases' => array('max_failed_payments')
      ),
      'CURRENCYCODE' => array(
        'aliases' => array('currency_code'),
        'filters' => array('strtoupper')
      ),
      'L_BILLINGTYPE0' => array(
        'aliases' => array('billing_type')
      ),
      'L_BILLINGAGREEMENTDESCRIPTION0' => array(
        'aliases' => array('ba_descr')
      ),
      'DESC' => array(
        'aliases' => array('ba_descr_conf')
      ),
      'L_PAYMENTTYPE' => array(
        'aliases' => array('payment_type')
      ),
      'BILLINGPERIOD' => array(
        'aliases' => array('billing_period')
      ),
      'BILLINGFREQUENCY' => array(
        'aliases' => array('billing_frequency')
      ),
      'PROFILESTARTDATE' => array(
        'aliases' => array('start')
      ),
      'TOTALBILLINGCYCLES' => array(
        'aliases' => array('payments_count')
      ),
      'PROFILENAME' => array(
        'aliases' => array('profile_name')
      ),
      'TOKEN' => array(
        'aliases' => array('token')
      ),
      'PAYERID' => array(
        'aliases' => array('payer_id')
      ),
      'COUNTRYCODE' => array(
        'aliases' => array('country_code')
      ),
      'ORIGID' => array(
        'aliases' => array('original_id')
      ),
      'OPTIONALTRX' => array(
        'aliases' => array('optional_trx'),
        'filters' => array('filterOptionalTrx')
      ),
      'INITAMT' => array(
        'aliases' => array('optional_trx_amt'),
        'filters' => array('filterAmt')
      ),
      'NAME' => array( // Name on the card
        'aliases' => array('name'),
        'filters' => array('ucwords')
      ),
      'ACCT' => array(
        'aliases' => array('account'),
        'filters' => array('filterAcct')
      ),
      'EXPDATE' => array(
        'aliases' => array('expiration'),
        'filters' => array('filterExpdate')
      ),
      'CVV2' => array(
        'aliases' => array('cvv2')
      ),
      'BILLINGAGREEMENTID' => array(
        'aliases' => array('baid')
      ),
      'FIRSTNAME' => array(
        'aliases' => array('first_name'),
        'filters' => array('ucwords')
      ),
      'LASTNAME' => array(
        'aliases' => array('last_name'),
        'filters' => array('ucwords')
      ),
      'EMAIL' => array(
        'aliases' => array('email'),
        'filters' => array('filterEmail')
      ),
      'STREET' => array(
        'aliases' => array('street')
      ),
      'CITY' => array(
        'aliases' => array('city')
      ),
      'STATE' => array(
        'aliases' => array('state')
      ),
      'ZIP' => array(
        'aliases' => array('zip_code')
      ),
      'COUNTRY' => array(
        'aliases' => array('country')
      ),
      'PHONENUM' => array(
        'aliases' => array('telephone')
      ),
      'COMMENT1' => array(
        'aliases' => array('comment1')
      ),
      'COMMENT2' => array(
        'aliases' => array('comment2')
      ),
      'ORDERDESC' => array(
        'aliases' => array('order_descr')
      ),
      'RETURNURL' => array(
        'aliases' => array('return_url')
      ),
      'CANCELURL' => array(
        'aliases' => array('cancel_url')
      ),
      'VERBOSITY' => array(
        'aliases' => array('verbosity'),
        'default' => 'MEDIUM',
        'common' => true
      ),
      'CUSTIP' => array(
        'aliases' => array('custip'),
        'default' => '',
        'common' => true
      ),
      'HDRIMG' => array(
        'aliases' => array('hdrimg')
      ),
      'LOGOIMG' => array(
        'aliases' => array('logo')
      ),
      'LANDINGPAGE' => array(
        'aliases' => array('landing_page'),
        'filters' => array('filterLandingPage')
      )
    ),

    'methods' => array(
      'setExpressCheckoutSaleTransaction' => array(
        'required' => array('METHOD', 'PAYMENTREQUEST_0_AMT', 'RETURNURL', 'CANCELURL'),
        'values' => array(
          'METHOD' => 'SetExpressCheckout',
          'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale'
        )
      ),
      'getExpressCheckoutDetails' => array(
        'required' => array('METHOD', 'TOKEN'),
        'values' => array(
          'METHOD' => 'GetExpressCheckoutDetails'
        )
      ),
      'doExpressCheckoutSaleTransaction' => array(
        'required' => array('METHOD', 'TOKEN', 'PAYERID'),
        'values' => array(
          'METHOD' => 'DoExpressCheckoutPayment'
        )
      ),
      'setExpressCheckoutRecurringProfile' => array(
        'required' => array('METHOD', 'L_BILLINGTYPE0', 'L_BILLINGAGREEMENTDESCRIPTION0', 'RETURNURL', 'CANCELURL'),
        'values' => array(
          'METHOD' => 'SetExpressCheckout',
          'L_BILLINGTYPE0' => 'RecurringPayments'
        )
      ),
      'addExpressCheckoutRecurringProfile' => array(
        'required' => array('METHOD', 'PROFILESTARTDATE', 'DESC', 'BILLINGPERIOD', 'BILLINGFREQUENCY', 'AMT'),
        'values' => array(
          'METHOD' => 'CreateRecurringPaymentsProfile'
        )
      ),
      'doCustomRequest' => array(
        'required' => array('METHOD')
      ),
    )
  );

  /**
   * Class constructor.
   */
  public function __construct($servlets = array())
  {
    parent::__construct($servlets);
    $this->config['params']['CUSTIP']['default'] =
      isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1');
  }

  /**
   * Allow for final params alteration before they go to submitter.
   *
   * This alteration is usually specific to the particular gateway
   * and all gateways must implement it even if they leave params untouched.
   *
   * @param array $params Origianl param array.
   *
   * @return array Modified/original array.
   */
  public function alterParamsForRequest($params) {
    return $params;
  }

  /**
   * Format response.
   *
   * @param  string $result Raw response from gateway.
   * @param  string $format Format identifier ('array' or 'object').
   * @return void
   */
  public function formatResponse($result, $format = 'array')
  {
    if (! empty($result)) {
      $response = $format == 'array' ? array() : new StdClass();
      $tokens = explode('&', $result);
      foreach ($tokens as $token) {
        $pair = explode('=', $token);
        if ($format == 'array') {
          $response[urldecode($pair[0])] = isset($pair[1]) ? urldecode($pair[1]) : null;
        } else {
          $response->{urldecode($pair[0])} = isset($pair[1]) ? urldecode($pair[1]) : null;
        }
      }
      return $response;
    }
  }

  /**
   * Filters
   */

  /**
   * PaymentAction filter (PAYMENTREQUEST_n_PAYMENTACTION).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterPaymentAction($value)
  {
    $value = strtolower($value);
    $value = ucwords($value);
    $value = trim($value);

    $allowed = array(
      'Sale',          // This is a final sale for which you are requesting payment (default).
      'Authorization', // This payment is a basic authorization subject to settlement with PayPal Authorization and Capture.
      'Order'          // This payment is an order authorization subject to settlement with PayPal Authorization and Capture.
    );

    if (! in_array($value, $allowed)) {
      return false;
    }

    return $value;
  }

  /**
   * Account Number filter (ACCT).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterAcct($value)
  {
    $value = preg_replace('/[^0-9]/', '', trim($value));
    if (! is_numeric($value)) {
      return false;
    }

    return (string) $value;
  }

  /**
   * Expiration Date filter (EXPDATE).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterExpdate($value)
  {
    $value = preg_replace('/[^0-9]/', '', trim($value));
    if (! is_numeric($value) || strlen($value) != 4) {
      return false;
    }

    return $value;
  }

  /**
   * Amount filter (PAYMENTREQUEST_0_AMT).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterAmt($value)
  {
    $value = str_replace(array('-', '_', ' ', '$', ','), array(''), $value);
    $value = trim($value);
    if (! is_numeric($value)) {
      return false;
    }

    return $value;
  }

  /**
   * Email filter (EMAIL).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterEmail($value)
  {
    $value = strtolower($value);
    return filter_var($value, FILTER_VALIDATE_EMAIL);
  }

  /**
   * Landing Page filter (LANDINGPAGE).
   *
   * @param  string $value Value to be checked
   * @return mixed         Corrected value or false if value is not acceptable.
   */
  protected function filterLandingPage($value)
  {
    $value = strtolower($value);
    $value = ucwords($value);
    $allowed = array(
      'Billing', // Non-PayPal account screen
      'Login'    // PayPal account login screen
    );

    $value = trim($value);
    if (! in_array($value, $allowed)) {
      return false;
    }

    return $value;
  }
}
