<?php

namespace SankyNet\API\Payments;

interface GatewayInterface
{
  public function setServlets($servlets);
  public function getServletUrl($environment);
  public function getValidParamName($key);
  public function filterParam($key, $value);
  public function isValidMethod($method);
  public function alterParamsForRequest($params);
  public function getMethodParams($method);
  public function validateMethodParams($method, $params);
  public function formatResponse($result, $format = array());
}
