<?php

namespace SankyNet\API\Payments;

abstract class Gateway
{

  /**
   * Class constructor.
   *
   * @param string $environment Environment identifier.
   * @param array  $gateways    Gateway urls's.
   */
  public function __construct($servlets = array())
  {
    $this->setServlets($servlets);
  }

  /**
   * Set servlets urls for each environment identifier.
   *
   * This method takes an array where the key in this array
   * is an environment identifier and the value is a servlet url.
   *
   * Example:
   * $servlets = array(
   *   'test'=>'https://pilot-payflowpro.paypal.com',
   *   'live'=>'https://payflowpro.paypal.com'
   * )
   *
   * @param array $servlets Servlets urls.
   */
  public function setServlets($servlets) {
    if (! empty($servlets)) {
      if (! empty($servlets['test'])) {
        $this->config['servlets']['test'] = $servlets['test'];
      }
      if (! empty($servlets['live'])) {
        $this->config['servlets']['live'] = $servlets['live'];
      }
    }
  }

  /**
   * Get Servlets Urls.
   *
   * @param  string $environment Environment identifier (usually 'test' or 'live').
   * @return string              Url string.
   */
  public function getServletUrl($environment) {
    return isset($this->config['servlets'][$environment]) ?
      $this->config['servlets'][$environment] : '';
  }

  /**
   * Check if a given parameter name is allowed.
   *
   * @param  string $key Parameter name.
   * @return mixed       Gateway specific parameter name
   *                     or false if parameter name in not valid.
   */
  public function getValidParamName($key)
  {
    foreach ($this->config['params'] as $pkey => $settings) {
      if ($key == $pkey || in_array($key, $settings['aliases'])) {
        return $pkey;
      }
    }
    return false;
  }

  /**
   * Filter a given param.
   *
   * Validate if a given param is allowed first
   * and then pass it through all available filters/validators.
   *
   * @param  string $key   Parameter name
   * @param  string $value Parameter value
   * @return mixed         Filtered param which is a name - value pair
   *                       where name is gateway-specific param name
   *                       or false if any filter returns false.
   */
  public function filterParam($key, $value)
  {
    if (($pkey = $this->getValidParamName($key)) !== false) {
      $value = $this->runFilters($pkey, $value);
      if ($value === false) {
        return false;
      }
      return array($pkey => $value);
    }
    return false;
  }

  /**
   * Get parameters that are common for any gateway call.
   *
   * @return array Common paramters array.
   */
  protected function getCommonParams()
  {
    $params = array();
    foreach ($this->config['params'] as $pkey => $settings) {
      if (! empty($settings['common']) && isset($settings['default'])) {
        $params[$pkey] = $settings['default'];
      }
    }
    return $params;
  }

  /**
   * Pass a given parameter through all available filters.
   *
   * @param  string $pkey  Main key from $map array
   *                       which is gateway-specific parameter name.
   * @param  string $value Value of the parameter
   * @return mixed         Filtered parameter value
   *                       or false if any filter returned false.
   */
  protected function runFilters($pkey, $value)
  {
    if (
      isset($this->config['params'][$pkey]['filters']) &&
      is_array($this->config['params'][$pkey]['filters'])
    ) {
      $filters = $this->config['params'][$pkey]['filters'];
      foreach ($filters as $filter) {
        $value = $this->invokeFilter($filter, $value);
        if ($value === false) {
          return false;
        }
      }
    }
    return $value;
  }

  /**
   * Call the filter function (if exists).
   *
   * @param  string $name  Name of the filter.
   * @param  mixed  $value Variable value that will be passed to the filter.
   * @return mixed         Modified value, false if filter cannot accept the value
   *                       or original value if filter does not exist.
   */
  private function invokeFilter($name, $value)
  {
    if (function_exists($name)) {
      return $name($value);
    } else if (method_exists($this, $name)) {
      return $this->$name($value);
    }
    return $value;
  }

  /**
   * Check if a given method name is valid.
   *
   * @param  string  $method Method name.
   * @return boolean         True if a given method is on the list of allowed API calls.
   *                         False otherwise.
   */
  public function isValidMethod($method)
  {
    $methods = array_keys($this->config['methods']);
    return in_array($method, $methods);
  }

  /**
   * Get all preconfigured, method-specific parameters.
   *
   * @param  string $method Method name.
   * @return array          Method-specific parameters.
   */
  public function getMethodParams($method)
  {
    $params = $this->getCommonParams();
    if (
      isset($this->config['methods'][$method]['values']) &&
      is_array($this->config['methods'][$method]['values'])
    ) {
      $params = array_merge($params, $this->config['methods'][$method]['values']);
    }
    return $params;
  }

  /**
   * Validate if given parameters are sufficient
   * to run a specific API request call.
   *
   * @param  string  $method Method name.
   * @param  array   $params Parameters array.
   * @return boolean         True if parameters are sufficient
   *                         to run a given API request call.
   */
  public function validateMethodParams($method, $params)
  {
    if ($this->isValidMethod($method)) {
      // Check the map.
      foreach ($this->config['params'] as $pkey => $settings) {
        if (! empty($settings['required'])) {
          if (! isset($params[$pkey])) {
            return false;
          }
        }
      }
      // Check method configuration.
      if (
        isset($this->config['methods'][$method]['required']) &&
        is_array($this->config['methods'][$method]['required'])
      ) {
        foreach ($this->config['methods'][$method]['required'] as $pkey) {
          if (! isset($params[$pkey])) {
            return false;
          }
        }
      }
      return true;
    }
    return false;
  }
}
