<?php

namespace SankyNet\API\Payments;

use SankyNet\Core\NVPSubmitter;

/**
 * Class description and usage.
 *
 * Public attributes:
 *   No direct attributes
 *   Attributes available through magic method __get:
 *     Main keys of self::$map array are available as attributes as well as
 *     values of the key 'field' if set in the subarrays for each main key.
 *
 * Public methods:
 *   __construct($params = array(), $environment = 'test')
 *     Class constructor.
 *     Examples:
 *       $params = array('vendor' => 'myvendor', 'partner' => 'paypal_partner', ...);
 *       $payflow = new PayFlow($params, 'live');
 *
 *   setEnvironment($environment = 'test')
 *     Specify 'test' or 'live' and define wheter PayFlow class should submit the request
 *     to the sandbox endpoint or live endpoint.
 *     You can also set environment passing it to the constructor. Default 'test'.
 *     Examples:
 *       $payflow->setEnvironment('live');
 *
 *   __set($param, $value)
 *     Set request parameters individually.
 *     Examples:
 *       $payflow->USER = 'SankySandboxAccount';
 *       $payflow->password = 'mySe8cre0t_password';
 *
 *   set($params = array())
 *     Set multiple parameters at a time.
 *     This method uses __set($param, $value) method.
 *     Examples:
 *       $params = array('vendor' => 'myvendor', 'partner' => 'paypal_partner', ...);
 *       $payflow->set($params);
 *
 *   __get($param)
 *     Get individual parameter.
 *     Examples:
 *       $myparam = $payflow->vendor;
 *
 *   getParams()
 *     Get all parameters that have been set.
 *
 *   getRequest()
 *     Get composed request string based on params currently set.
 *
 *   getResponse()
 *     Get response after calling wrapper methods (see below) in array or object format.
 *
 *   getStatus()
 *     Get an array containing all errors, exceptions and warning messages
 *     set during lifecycle of the PayFlow object.
 *
 * Wrapper specific methods (see self::$configs array for details):
 *   doCreditCardSaleTransaction();
 *
 * Example workflow:
 *   $params_connection = array(
 *     'vendor' => 'my_username',
 *     'partner' => 'PayPal',
 *     'user' => 'my_username',
 *     'password' => 'my_password',
 *   );
 *
 *   $params_transaction = array(
 *     'account' => '3782-8224_631-0005',
 *     'expiration' => '1215',
 *     'amount' => '$ 1,200.76',
 *   );
 *
 *   $payflow = new PayFlow($params_connection, 'live');
 *   $payflow->set($params_transaction);
 *   $result = $payflow->doCreditCardSaleTransaction();
 *   if ($result) {
 *     $response = $payflow->getResponse();
 *     // process response...
 *   } else {
 *     // something went wrong so analyze the status...
 *     echo '<pre>' . print_r($payflow->getStatus(), true) . '</pre>';
 *   }
 */

/**
 *
 */
class GatewayController
{

  private $gateway;               // Gateway object.
  private $environment;           // Usually 'test' or 'live'.
  private $submitter = null;      // Submitter object.
  private $params    = array();   // Holds request parameters (filtered and ready for submission).
  private $response  = '';        // Holds formatted gateway response.
  private $error;                 // Status array.

  /**
   * Class constructor.
   *
   * @param object $gateway     Gateway object
   * @param string $environment Environment identifier.
   * @param object $submitter   Option alternative submitter
   *                            through which the request will be sent.
   */
  public function __construct($gateway, $environment = 'test', &$submitter = null)
  {
    $this->setGateway($gateway);
    $this->setEnvironment($environment);
    $this->setSubmitter($submitter);
  }

  /**
   * Setter magic method.
   *
   * @param string $param Parameter name.
   * @param mixed  $value Parameter value.
   */
  public function __set($key, $value)
  {
    if ($this->isValidGateway()) {
      $param = $this->gateway->filterParam($key, $value);
      if ($param !== false) {
        $this->params = array_merge($this->params, $param);
      }
    }
  }

  /**
   * Get the value of the property.
   *
   * @param  string $property Property name.
   * @return mixed            Value of the property.
   */
  public function __get($key)
  {
    if ($this->isValidGateway()) {
      $mkey = $this->gateway->getValidParamName($key);
      if ($mkey !== false) {
        return isset($this->params[$mkey]) ? $this->params[$mkey] : null;
      }
    }

    $trace = debug_backtrace();
    trigger_error(
      'Undefined property via __get(): ' . $key .
      ' in ' . $trace[0]['file'] .
      ' on line ' . $trace[0]['line'],
      E_USER_NOTICE);
    return null;
  }

  /**
   * Magic method for executing wrapper methods.
   *
   * @param  string $name      Wrapper method name.
   * @param  mixed  $arguments Any additional arguments.
   * @return mixed             Null if method not found otherwise true or false
   *                           depending on the result of the execution.
   */
  public function __call($name, $arguments)
  {
    $result = false;
    if ($this->isValidGateway()) {
      if ($this->gateway->isValidMethod($name)) {
        $this->set($this->gateway->getMethodParams($name));
        if ($this->gateway->validateMethodParams($name, $this->params)) {
          $servlet = $this->gateway->getServletUrl($this->environment);
          $this->submitter->setServlet($servlet);
          $params = $this->gateway->alterParamsForRequest($this->params);
          $this->submitter->set($params);
          try {
            $result = $this->submitter->sendRequest();
          } catch (Exception $e) {
            $this->error = $e->getCode() . ' - ' . $e;
          }
          if ($result) {
            $response = $this->submitter->getResponse();
            $this->response = $this->gateway->formatResponse($response);
            return true;
          }
        } else {
          $this->error = 'Missing or invalid prameters to execute ' . $name . ' method.';
        }
      } else {
        $trace = debug_backtrace();
        trigger_error(
          'Undefined method via __call(): ' . $name .
          ' in ' . $trace[0]['file'] .
          ' on line ' . $trace[0]['line'],
          E_USER_NOTICE);
        return null;
      }
    } else {
      $this->error = 'Invalid or not defined gateway.';
    }
    return false;
  }

  /**
   * Set gateway.
   *
   * @param object $gateway Gateway object.
   */
  public function setGateway($gateway) {
    $this->gateway = $gateway;
  }

  /**
   * Set environment (test or live) for the class.
   *
   * @param string $environment Environment identifier.
   */
  public function setEnvironment($environment = 'test')
  {
    $environment = strtolower($environment);
    if ($environment == 'test' || $environment == 'live') {
      $this->environment = $environment;
    }
  }

  /**
   * Set alternative submitter service.
   * When this method is called internal sumitter
   * is reinstantiated.
   *
   * @param object $submitter Submitter
   */
  public function setSubmitter($submitter)
  {
    if (is_object($submitter)) {
      $this->submitter = $submitter;
    } else {
      $this->submitter = new NVPSubmitter();
    }
  }

  /**
   * Check if a gateway was initialized.
   *
   * @return boolean
   */
  private function isValidGateway() {
    return is_object($this->gateway);
  }

  /**
   * Set request parameters.
   *
   * @param array $params An array of the params.
   */
  public function set($params = array(), $clear = false)
  {
    if ($clear) {
      $this->params = array();
    }
    if (is_array($params) || is_object($params)) {
      foreach ($params as $key => $value) {
        $this->__set($key, $value);
      }
    }
  }

  /**
   * Return an array of params.
   *
   * @return array Params array.
   */
  public function getParams()
  {
    return $this->params;
  }

  /**
   * Get last wrapper method call response.
   *
   * @return mixed Formatted response (array or object).
   */
  public function getResponse()
  {
    return $this->response;
  }

  public function getError() {
    return $this->error;
  }

}
