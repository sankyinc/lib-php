# SankyNet API

API Wrappers for various third party vendors

Available Classes:

+ [`SankyNet\API\ConstantContact`](#constantcontact)
+ [`SankyNet\API\Silverpop`](#silverpop)
+ [`SankyNet\API\PayFlow`](#payflow)

## Silverpop

Instantiation:

```php
require 'vendor/autoload.php';

use SankyNet\API\Silverpop;
use SankyNet\Core\User;

$user_name  = "carrie@sankynet.com";
$password   = "password";
$host       = "http://api5.silverpop.com/XMLAPI";
$connection = new Silverpop($user_name, $password, $host, '1372821');
```

**getListMetaData**
```php
$connection->getListMetaData('12345');
```


**addOrUpdateRecipient**
```php
$matt = new User;
$matt->email = "mryan@sanky.info";
$matt->city = "New York City";
$matt->state = "NY";
$matt->__set("address line 1", "527 7th Ave");

$connection->addOrUpDateRecipient($matt);
```

**selectRecipientData**

Accepts an email and returns `User` object. Arbitrary fields are accessed via `__get()` like so: `User->__get('street address 1')`

```php
$matt = new User;
$matt->email = "mryan@sanky.info";

$connection->selectRecipientData($matt->email);
echo '<pre>'; print_r($connection->response); echo '</pre>';

/*
 *SankyNet\Core\User Object
 *(
 *    [properties:SankyNet\Core\Model:private] => Array
 *        (
 *            [id] => 42962205010
 *            [Address Line 1] => 527 7th Ave
 *            [Address Line 2] =>
 *            [Append Date] =>
 *            [CHP Hospital] => BI
 *            [CHP ID Number] =>
 *            [City] => New York City
 *            [Contact Type] =>
 *            [First Name] =>
 *            [Last DM Gift Amt] =>
 *            [Last DM Gift Date] =>
 *            [Last DM Gift Source] =>
 *            [Last Name] =>
 *            [Last Online Gift Amt] =>
 *            [Last Online Gift Date] =>
 *            [Send Hour] => 0
 *            [State] => NY
 *            [Zip Code] =>
 *        )
 *
 *)
 */

echo $connection->response->__get("CHP Hostpital"); // "BI"
```

**addContactToContactList**
```php
$matt = new User;
$matt->email = "mryan@sanky.info";
$matt->city = "New York City";
$matt->state = "NY";
$matt->__set("address line 1", "527 7th Ave");

$sp_user = $connection->selectRecipientData($matt->email);
$connection->addContactToContactList($sp_user->id, '12345');
```

## ConstantContact

Instantiation:

```php
require 'vendor/autoload.php';

use SankyNet\API\ConstantContact;
use SankyNet\Core\User;

$api_key       = 'xxxxxxxxxxxxxxxxxxxxxxxx';
$access_token  = 'xxxxxxxx-8eb3-4ff6-9f1b-870e706138c1';
$cc            = new ConstantContact($api_key, $access_token);
```

**addOrUpdateContact**
```php
$user = new User;
$user->first_name = 'Matthew';
$user->last_name = 'Ryan';
$user->middle_name = "James";
$user->email = 'mryan@sanky.info';
$user->phone = '212-999-3500';
$user->company_name = 'SankyNet';
$user->job_title = 'Tech Director';
$user->line1 = '599 11th Ave.';
$user->line2 = '6th Floor';
$user->city = 'New York City';
$user->zip = '10036';
$user->state = 'NY';
$user->country = 'US';

$list = '1';

$returnContact = $this->cc->addOrUpdateContact($user, $list);
```

## PayFlow

Example workflow:

```php
$params_connection = array(
 'vendor' => 'my_username',
 'partner' => 'PayPal',
 'user' => 'my_username',
 'password' => 'my_password',
);

$params_transaction = array(
 'account' => '3782-8224_631-0005',
 'expiration' => '1215',
 'amount' => '$ 1,200.76',
);

$payflow = new PayFlow($params_connection, 'live');
$payflow->set($params_transaction);
$result = $payflow->doCreditCardSaleTransaction();
if ($result) {
 $response = $payflow->getResponse();
 // process response...
} else {
 // something went wrong so analyze the status...
 echo '<pre>' . print_r($payflow->getStatus(), true) . '</pre>';
}
```
