<?php

namespace SankyNet\API;

use SankyNet\Core\User;
use Ctct\ConstantContact as Ctct;
use Ctct\Components\Contacts\Contact;
use Ctct\Components\Contacts\Address;
use Ctct\Components\Contacts\ContactList;
use Ctct\Components\Contacts\EmailAddress;
use Ctct\Exceptions\CtctException;

/**
 * Constant Contact
 *
 * Simple wrapper class for CC API.
 *
 * @package Sanky
 * @version dev-master
 * @author m@mrevd.me
 */
class ConstantContact
{
  private $api_key;
  private $access_token;
  private $cc;

  /**
   * __construct
   *
   * @param mixed $api_key
   * @param mixed $access_token
   * @access public
   * @return void
   */
  public function __construct($api_key, $access_token)
  {
    if (is_null($api_key) or is_null($access_token)) {
      throw new \InvalidArgumentException;
    }

    $this->api_key = $api_key;
    $this->access_token = $access_token;

    $this->cc = new Ctct($this->api_key);
  }

  /**
   * addOrUpdateContact
   *
   * @param User $user
   * @param mixed $list
   * @access public
   * @return void
   */
  public function addOrUpdateContact(User $user, $list)
  {
    try {
      /**
       * check to see if a contact with the email addess already exists in the account
       */
      $response = $this->cc->getContactByEmail($this->access_token, $user->email);

      /**
       * create a new contact if one does not exist
       */
      if (empty($response->results)) {
        $contact = new Contact();
        $contact->addEmail($user->email);
      } else {
        /**
         * update the existing contact if address already existed
         */
        $contact = $response->results[0];
      }

      $contact->addList($list);
      $contact->first_name = $user->first_name;
      $contact->middle_name = $user->middle_name;
      $contact->last_name = $user->last_name;
      $contact->home_phone = $user->phone;
      $contact->company_name = $user->company_name;
      $contact->job_title = $user->job_title;

      $address = new Address();
      $address->line1 = $user->line1;
      $address->line2 = $user->line2;
      $address->line3 = $user->line3;
      $address->city = $user->city;
      $address->postal_code = $user->zip;
      $address->state_code = $user->state;
      $address->country_code = $user->country;
      $address->address_type = $user->address_type;

      /**
       * reset addresses before adding a new one
       */
      $contact->addresses = null;
      $contact->addAddress($address);

      /**
       * either add new or update existing
       */
      if (empty($response->results)) {
        return $returnContact = $this->cc->addContact($this->access_token, $contact, true);
      } else {
        return $returnContact = $this->cc->updateContact($this->access_token, $contact, true);
      }
    } catch (CtctException $ex) {
      /**
       * catch any exceptions thrown during the process and return the errors
       */
      return $ex->getErrors();
    }
  }

  /**
   * getLists
   *
   * @access public
   * @return void
   */
  public function getLists()
  {
    try{
      return $lists = $this->cc->getLists($this->access_token);
    } catch (CtctException $ex) {
      return $ex->getErrors();
    }
  }
}
